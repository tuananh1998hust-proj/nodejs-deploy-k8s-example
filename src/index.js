const express = require('express');

const app = express();

app.get('/', function(req, res) {
  res.status(200).json({
    data: 'Hello K8s'
  });
});

app.listen(3000, () => console.log('server is start on port 3000'));
